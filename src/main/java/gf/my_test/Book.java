package main.java.gf.my_test;

public class Book implements BookInterface{
	
	int bookPagesNumber;
	int currentPage;
	
	public Book(int bookPagesNumber)  {
		if(bookPagesNumber < 1){
			//Unchecked exceptions: Runtime Exceptions
			throw new IllegalArgumentException("The pages number of the book must be greater than 0");
		}else{
			this.bookPagesNumber = bookPagesNumber;
			this.currentPage = 1;
		}
	}
	
	public boolean scrollForward(){
		if(currentPage + 1 <= bookPagesNumber){
			currentPage++;
			return  true;
		}else{
			return false;
		}
	}

	public int getBookPagesNumber() {
		return bookPagesNumber;
	}

	public void setBookPagesNumber(int bookPagesNumber) {
		this.bookPagesNumber = bookPagesNumber;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

}
